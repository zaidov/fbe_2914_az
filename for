#include <iostream>
#include <Windows.h>
using namespace std;

int main()
{
	// for - dovr konstruksiyasi
	// for(iteratorun elan edilmesi, iteratorun dayanma sherti, iteratorun deyishme addimi)
	// for(START, STOP, STEP)
	// iterasiya - kodun bir defe tekrarlanmasi
	// iterator - her tekrarda qiymeti deyishilen deyishen
	
	// Ekrana ulduzlardan ibaret kvadrat cap edin
	// Kvadratin terefini istifadeci daxil edir

	int teref;
	cin >> teref;
	for (int i = 0; i < teref; i++)
	{
		cout << "i: " << i << "\t";
		for (int j = 0; j < teref; j++)
		{
			if (i == teref - 1
				|| i + j >= teref - 1) cout << "* ";
			else cout << "  ";
			Sleep(50);
		}
		for (int j = 0; j < teref; j++)
		{
			if ( (i == teref - 1 && j != teref - 1)
				|| i == j + 1
				|| i >= j
				) cout << "* ";
			else cout << "  ";
			Sleep(5);
		}
		cout << endl;
	}

	//int teref;
	//cin >> teref;
	//for (int i = 0; i < teref; i++)
	//{
	//	cout << "i: " << i << "\t";
	//	for (int j = 0; j < teref; j++)
	//	{
	//		if (i == 0
	//			|| i == teref - 1
	//			|| j == 0
	//			|| j == teref - 1
	//			|| i == j
	//			|| i + j == teref - 1) cout << "* ";
	//		else cout << "  ";
	//		Sleep(50);
	//	}
	//	cout << endl;
	//}

	//for (int i = 0; i < 10; i++)
	//{
	//	cout << "i: " << i << " ";
	//	for (int j = 0; j < 10; j++)
	//	{
	//		cout << "j: " << j << " ";
	//	}
	//	cout << endl;
	//}

	//for (int i = 0; i < 150; i++)
	//{
	//	if (i % 2 == 0) cout << i << endl;
	//}

	//for (int i = 150; ; i--) {
	//	cout << i << endl;
	//	if (i == 0) break;
	//}

	//for (;;) { // sonsuz dovr
	//	cout << "Salam\n";
	//}

	// Ekrana ulduzlardan ibaret duz xett cap edin. Xettin uzunlugunu istifadeci daxil edir
	//int size;
	//cout << "Xettin uzunlugunu daxil edin: "; cin >> size;
	//for (int i = size; i > 0; i--)
	//{
	//	cout << "*";
	//}

	//cout << "1-150 araliginda 13-e bolunen ededler: ";
	//for (int i = 1; i < 150; i++)
	//{
	//	if (i % 13 == 0) cout << i << " ";
	//}
	//cout << endl;

	//cout << "1-150 araliginda 6-a bolunen ededler: ";
	//for (int i = 1; i < 150; i++)
	//{
	//	if (i % 6 == 0) cout << i << " ";
	//}
	//cout << endl;

	//cout << "1-150 araliginda hem 13 hem de 6-a bolunen ededler: ";
	//for (int i = 1; i < 150; i++)
	//{
	//	if (i % 13 == 0 && i % 6 == 0) cout << i << " ";
	//}

	//int count = 0;
	//for (int i = 1; i < 150; i++)
	//{
	//	if (i % 13 == 0 && i % 6 == 0) count++;
	//}
	//cout << "1-150 araliginda 13-e bolunen ededlerin sayi: " << count << endl;

	//int sum = 0, mult = 1;
	//for (int i = 1; i < 20; i++)
	//{
	//	if(i % 2 == 0) sum += i;
	//	if(i % 2 != 0) mult *= i;
	//}

	//cout << "Cem: " << sum << endl;
	//cout << "Hasil: " << mult << endl;



	//for (int i = 0; i < 100; i++)
	//{
	//	cout << i << endl;
	//	i += 5;
	//}


	//int n = 50;
	//for (int i = 0; i < n; i++)
	//{

	//}

	//for (int number = 0; number < 50; number++)
	//{
	//	cout << number << endl;
	//}

	//for (int number = 0; number < 10; number++) {
	//	cout << number << endl;
	//}

	//int number = 0;
	//while (number < 10) {
	//	cout << number << endl;
	//	number++;
	//} // kod nece defe tekrarlanirsa, o qeder de iterasiya bash verir

}